package com.hotelmgt.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity(name = "user")
public class User extends AbstractProperty{
	
	public User() {
	}

	public User(String phoneNumber, String password, List<Role> roles) {
		this.phoneNumber = phoneNumber;
		this.password = password;
		this.roles = roles;
	}

	private static final long serialVersionUID = 1L;
	
	@Column(unique = true)
	private String phoneNumber;
	
	private String password;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "id", unique = true)
	private List<Role> roles;

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	@Override
	public String toString() {
		return "User [phoneNumber=" + phoneNumber + ", password=" + password + ", roles=" + roles + "]";
	}


}
