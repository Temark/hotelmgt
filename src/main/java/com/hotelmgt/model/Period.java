package com.hotelmgt.model;

import java.time.LocalDateTime;

import javax.persistence.Entity;


@Entity(name = "period")
public class Period extends AbstractProperty{

	private static final long serialVersionUID = 1L;
	
	public LocalDateTime startDatetime;
	public LocalDateTime endDatetime;
	
	
	public LocalDateTime getStartDatetime() {
		return startDatetime;
	}

	public void setStartDatetime(LocalDateTime startDatetime) {
		this.startDatetime = startDatetime;
	}

	public LocalDateTime getEndDatetime() {
		return endDatetime;
	}

	public void setEndDatetime(LocalDateTime endDatetime) {
		this.endDatetime = endDatetime;
	}

	@Override
	public String toString() {
		return "Period [startDatetime=" + startDatetime + ", endDatetime=" + endDatetime + "]";
	}
	
	
}
