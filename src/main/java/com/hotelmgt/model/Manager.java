package com.hotelmgt.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity(name = "manager")
public class Manager extends AbstractProperty{
	
	public Manager() {
	}

	public Manager(Person person) {
		this.person = person;
	}

	private static final long serialVersionUID = 1L;
	
	@JoinColumn(name = "id", unique=true)
	@OneToOne(cascade = CascadeType.ALL)
	private Person person;
	

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	@Override
	public String toString() {
		return "Receptionist [person=" + person + "]";
	}
	
}
