package com.hotelmgt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hotelmgt.model.Person;
import com.hotelmgt.model.User;
import java.lang.String;
import java.util.List;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {

	
//	@Query(value = "select u from User u where u.phoneumber = :phonenumber")
//	User getUserByPhoneNumber(@Param("phonenumber") String phonenumber);
}