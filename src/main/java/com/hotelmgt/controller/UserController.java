package com.hotelmgt.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;

import com.hotelmgt.model.Person;
import com.hotelmgt.model.User;
import com.hotelmgt.service.PersonService;
import com.hotelmgt.service.UserService;

@Controller
public class UserController {
	
	@Autowired
	private UserService userService;

	
	@Autowired
	private PersonService personService;
	
	@GetMapping(value="/")
    public String index() {
        return "login";
    }
	
	@GetMapping(value="/form")
    public String form() {
        return "form";
    }
	
	@GetMapping(value="/user")
    public String user(Model model) {
		model.addAttribute("user", new User());
		
        return "user";
        
       
    }
	
	@GetMapping(value="/signup")
    public String signUp(Model model) {
		model.addAttribute("user", new User());
        return "signup";
    }
	
	@PostMapping(value="/register")
    public String register(@ModelAttribute("user") User user) {
		System.out.println(user.toString());
		userService.registerUser(user);
		return "welcome";     
    }
	
	@PostMapping(value="/auth")
    public String auth(WebRequest webRequest) {
		String phoneNumber = webRequest.getParameter("phoneNumber");
		String password = webRequest.getParameter("password");
		return userService.authenticateUser(phoneNumber, password);     
    }
    
	@GetMapping(value="/profile")
    public String register(Model model) {
		model.addAttribute("person", new Person());
		return "UserProfile";   
    }
	
	@PostMapping(value="/profile")
    public String registered(@ModelAttribute("person") Person person) {
		personService.registerPerson(person);
		return "redirect:/";
		
		   
    }
}


