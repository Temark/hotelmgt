package com.hotelmgt.service;

import com.hotelmgt.model.User;

public interface UserService {
	
	String authenticateUser( String phoneNumber, String password);

	User registerUser(User user);

	
}
