package com.hotelmgt.service;

import java.security.CryptoPrimitive;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.hotelmgt.model.Person;
import com.hotelmgt.model.Role;
import com.hotelmgt.model.User;
import com.hotelmgt.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	PasswordEncoder encode;
	
	@Override
	public String authenticateUser(String phoneNumber, String password) {
	
		String checkUserName = null;
		String checkUserPassword = null;
		List<User> userAuth= userRepository.findByPhoneNumber(phoneNumber);

		if (userAuth.isEmpty() || userAuth.isEmpty()) {
			return "failed";
		}
		else {
			checkUserName = userAuth.get(0).getPhoneNumber().trim();
			checkUserPassword = userAuth.get(0).getPassword().trim();
			
			if((phoneNumber.equalsIgnoreCase(checkUserName)) && (encode.matches(password, checkUserPassword))) {				
				return "welcome";
			}
			return "failed";
			
		}
	}


	@Override
	public User registerUser(User user) {
//		
		String phoneNoTrim = user.getPhoneNumber().trim();
		String encodedPassword = encode.encode(user.getPassword());
		LocalDateTime dateCreated = user.getDateCreated();
		LocalDateTime dateModified = user.getDateModified();
		System.out.println("Password " + encodedPassword);
		
		user.setPhoneNumber(phoneNoTrim);
		user.setPassword(encodedPassword);
		user.setDateCreated(dateCreated);
		user.setDateModified(dateModified);
		return userRepository.save(user );
		
	}



}